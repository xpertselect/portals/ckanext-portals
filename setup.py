# encoding: utf-8


from setuptools import setup, find_packages
from codecs import open
from os import path


with open(path.join(path.abspath(path.dirname(__file__)), 'README.md'),
          encoding='utf-8') as f:
    long_description = f.read()


setup(
    name='''ckanext-portals''',
    version='1.0.0',
    description='''A CKAN extension that implements a dataset schema based on 
                   DCAT-AP-DONL 1.1 (Finely tuned for the XpertSelect Portals 
                   stack) into CKAN.''',
    long_description=long_description,
    url='https://gitlab.com/xpertselect/portals/ckanext-portals',
    author='''Textinfo B.V.''',
    author_email='''support@textinfo.nl''',
    license='MIT',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'License :: Proprietary',
        'Programming Language :: Python :: 2.7',
    ],
    keywords='''ckan, extension, plugin, textinfo, data, dcat, dcat-ap-donl, 
                xpertselect, portals, opendata, open''',
    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
    namespace_packages=['ckanext'],
    install_requires=[],
    include_package_data=True,
    package_data={},
    data_files=[],
    entry_points='''
        [ckan.plugins]
        portals-scheme=ckanext.portals.plugins:SchemaPlugin
        portals-authorization=ckanext.portals.plugins:AuthorizationPlugin
        portals-interface=ckanext.portals.plugins:InterfacePlugin
    '''
)
