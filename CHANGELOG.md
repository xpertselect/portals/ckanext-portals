# Changelog

## Unreleased (`dev-main`)

### Added

### Changed

### Fixed

___

## Release 1.3.0

### Changed

- The `in_list` validator now caches a list in memory for a maximum of one day.

___

## Release 1.2.0

### Added

- The `notes_stripped` field now contains the `notes` attribute in which Markdown is first converted to HTML before it is stripped of any HTML markup.

___

## Release 1.1.1

### Fixed

- Stripping the HTML from a text field will no longer lead to encoding errors.

___

## Release 1.1.0

### Added

- Datasets (packages) will now be sent to Apache Solr with an additional `notes_stripped` field containing the `notes` attribute stripped of any HTML markup.
