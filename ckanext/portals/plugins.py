# encoding: utf-8


import ckan.plugins as plugins
import ckan.plugins.toolkit as tk
from ckanext.portals.logic import converters, validators
from ckanext.portals.logic.schemas import dcat_ap_donl
from ckanext.portals.logic.helpers import transformers
from ckanext.portals.logic.helpers.queries import wildcard_search
from ckanext.portals.logic.authorizations import dataset_purge_authorization


class SchemaPlugin(plugins.SingletonPlugin, tk.DefaultDatasetForm):
    plugins.implements(plugins.IValidators)
    plugins.implements(plugins.IDatasetForm)
    plugins.implements(plugins.IPackageController, inherit=True)

    # IValidators

    def get_validators(self): # noqa
        return {
            'convert_list_to_string': converters.convert_list_to_string,
            'convert_string_to_list': converters.convert_string_to_list,
            'default_conversion': converters.default,
            'single_value': validators.single_valued,
            'multi_value': validators.multi_valued,
            'is_string': validators.string,
            'is_bool': validators.boolean,
            'is_uri': validators.uri,
            'is_date': validators.date,
            'is_number': validators.number,
            'in_list': validators.in_list,
            'contact_point': validators.contact_point,
            'temporal': validators.temporal,
            'date_planned': validators.date_planned,
            'legal_foundation': validators.legal_foundation,
            'checksum': validators.checksum,
            'rights': validators.rights,
            'spatial': validators.spatial,
            'epsg_28992': validators.epsg_28992,
            'postcode_huisnummer': validators.postcode_huisnummer
        }

    # IDatasetForm

    def is_fallback(self): # noqa
        return True

    def package_types(self): # noqa
        return []

    def create_package_schema(self):
        schema = super(SchemaPlugin, self).create_package_schema()
        schema = dcat_ap_donl.create_schema(schema)

        return schema

    def update_package_schema(self):
        schema = super(SchemaPlugin, self).update_package_schema()
        schema = dcat_ap_donl.update_schema(schema)

        return schema

    def show_package_schema(self):
        schema = super(SchemaPlugin, self).show_package_schema()
        schema = dcat_ap_donl.show_schema(schema)

        return schema

    # IPackageController

    def before_index(self, data_dict): # noqa
        data_dict = transformers.md_to_html(data_dict)
        data_dict = transformers.strip_html(data_dict)

        return transformers.transform_multivalued_properties(data_dict)

    def after_show(self, context, data_dict): # noqa
        return context, transformers.remove_properties(data_dict)


class InterfacePlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IConfigurer)
    plugins.implements(plugins.ITemplateHelpers)
    plugins.implements(plugins.IFacets, inherit=True)

    # IConfigurer

    def update_config(self, config): # noqa
        tk.add_template_directory(config, 'templates')
        tk.add_resource('fanstatic', 'portals')

    # ITemplateHelpers

    def get_helpers(self): # noqa
        return {
            'wildcard_search': wildcard_search
        }

    # IFacets

    def dataset_facets(self, facets_dict, package_type): # noqa
        facets_dict.pop('organization', None)
        facets_dict['organization'] = tk._('CKAN Organization')

        return facets_dict


class AuthorizationPlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IAuthFunctions)

    # IAuthFunctions

    def get_auth_functions(self): # noqa
        return {
            'dataset_purge': dataset_purge_authorization
        }
