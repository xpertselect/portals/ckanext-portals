# encoding: utf-8


from datetime import datetime
import os
import json
import urllib2


def get_config(config_name):
    return ckanext_portals_config.get(config_name)


def in_list(name, value):
    return value in _get_list_contents(name)


def _get_list_contents(name): # noqa
    if not name in ckanext_portals_list_map.keys():
        return []

    current_date = datetime.strftime(datetime.now(), '%Y-%m-%d')

    if (not ckanext_portals_list_map[name]['date'] == current_date
        or ckanext_portals_list_map[name]['data'] == []):
        ckanext_portals_list_map[name]['date'] = current_date
        ckanext_portals_list_map[name]['data'] = _load_list(name)

    return ckanext_portals_list_map[name]['data']


def _load_list(list_name):
    if list_name not in ckanext_portals_list_map.keys():
        return []

    try:
        url = 'https://waardelijsten.dcat-ap-donl.nl/{0}'.format(
            ckanext_portals_list_map[list_name]['source'])
        ckanext_portals_list = json.loads(urllib2.urlopen(url).read())

        return ckanext_portals_list.keys()
    except urllib2.HTTPError:
        return []
    except urllib2.URLError:
        return []
    except KeyError:
        return []
    except Exception:
        return []


def _load_config_file():
    filepath = os.path.join(
        os.path.dirname(__file__),
        '..', '..', '..', '..', 'config.json'
    )

    with open(filepath, 'r') as config_file:
        contents = json.load(config_file)
        
    return contents


ckanext_portals_config = _load_config_file()
ckanext_portals_list_map = {
    'ADMS:Changetype': {
        'source': 'adms_changetype.json',
        'date': None,
        'data': []
    },
    'ADMS:DistributieStatus': {
        'source': 'adms_distributiestatus.json',
        'date': None,
        'data': []
    },
    'DONL:Catalogs': {
        'source': 'donl_catalogs.json',
        'date': None,
        'data': []
    },
    'DONL:DistributionType': {
        'source': 'donl_distributiontype.json',
        'date': None,
        'data': []
    },
    'DONL:Language': {
        'source': 'donl_language.json',
        'date': None,
        'data': []
    },
    'DONL:License': {
        'source': 'donl_license.json',
        'date': None,
        'data': []
    },
    'DONL:Organization': {
        'source': 'donl_organization.json',
        'date': None,
        'data': []
    },
    'IANA:Mediatypes': {
        'source': 'iana_mediatypes.json',
        'date': None,
        'data': []
    },
    'MDR:FiletypeNAL': {
        'source': 'mdr_filetype_nal.json',
        'date': None,
        'data': []
    },
    'Overheid:DatasetStatus': {
        'source': 'overheid_dataset_status.json',
        'date': None,
        'data': []
    },
    'Overheid:Frequency': {
        'source': 'overheid_frequency.json',
        'date': None,
        'data': []
    },
    'Overheid:Openbaarheidsniveau': {
        'source': 'overheid_openbaarheidsniveau.json',
        'date': None,
        'data': []
    },
    'Overheid:SpatialScheme': {
        'source': 'overheid_spatial_scheme.json',
        'date': None,
        'data': []
    },
    'Overheid:SpatialGemeente': {
        'source': 'overheid_spatial_gemeente.json',
        'date': None,
        'data': []
    },
    'Overheid:SpatialKoninkrijksdeel': {
        'source': 'overheid_spatial_koninkrijksdeel.json',
        'date': None,
        'data': []
    },
    'Overheid:SpatialProvincie': {
        'source': 'overheid_spatial_provincie.json',
        'date': None,
        'data': []
    },
    'Overheid:SpatialWaterschap': {
        'source': 'overheid_spatial_waterschap.json',
        'date': None,
        'data': []
    },
    'Overheid:Taxonomiebeleidsagenda': {
        'source': 'overheid_taxonomiebeleidsagenda.json',
        'date': None,
        'data': []
    }
}
