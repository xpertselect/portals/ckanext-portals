# encoding: utf-8

import re
import markdown


from config import get_config


def transform_multivalued_properties(data_dict):
    transformation = get_config('transformation')

    def remove_brackets(value):
        return value.replace('{', '').replace('}', '')

    for prop in transformation['package']['multivalued_field']:
        try:
            data_dict[prop] = remove_brackets(data_dict[prop]).split(',')
        except KeyError:
            continue

    for prop in transformation['package']['date_fields']:
        try:
            data_dict[prop] = '{0}Z'.format(data_dict[prop])
        except KeyError:
            continue

    for prop in transformation['resource']['multivalued_field']:
        try:
            for resource in data_dict['resources']:
                resource[prop] = remove_brackets(resource[prop])
        except KeyError:
            continue

    for prop in transformation['resource']['date_fields']:
        try:
            for resource in data_dict['resources']:
                resource[prop] = '{0}Z'.format(resource[prop])
        except KeyError:
            continue

    return data_dict


def md_to_html(data_dict):
    transformation_config = get_config('transformation')

    for transformation in transformation_config['package']['md_to_html_fields']:
        try:
            data_dict[transformation['target']] = markdown.markdown(data_dict[transformation['source']])
        except KeyError:
            continue

    return data_dict

def strip_html(data_dict):
    transformation_config = get_config('transformation')

    for transformation in transformation_config['package']['strip_html_fields']:
        try:
            data_dict[transformation['target']] = re.sub('<[^<]+?>', '', data_dict[transformation['source']])
        except KeyError:
            continue    

    return data_dict


def remove_properties(data_dict):
    properties_to_remove = get_config('properties_to_remove')
    [data_dict.pop(prop, None) for prop in properties_to_remove['package']]

    try:
        [[resource.pop(prop, None) for prop in properties_to_remove['resource']]
         for resource in data_dict['resources']]
    except KeyError:
        pass

    try:
        [[tag.pop(prop, None) for prop in properties_to_remove['tag']]
         for tag in data_dict['tags']]
    except KeyError:
        pass

    try:
        [data_dict['organization'].pop(prop, None)
         for prop in properties_to_remove['organization']
         if prop in data_dict['organization']]
    except KeyError:
        pass

    try:
        [[group.pop(prop, None) for prop in properties_to_remove['group']]
         for group in data_dict['groups']]
    except KeyError:
        pass
