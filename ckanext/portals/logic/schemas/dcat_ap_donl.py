# encoding: utf-8


from ckan.plugins.toolkit import get_validator, get_converter


def create_schema(original_schema):
    return _mutate_schema(original_schema, ':created')


def update_schema(original_schema):
    return _mutate_schema(original_schema, ':updated')


def show_schema(original_schema):
    mandatory = get_validator('not_empty')
    recommended = get_validator('ignore_empty')
    optional = get_validator('ignore_empty')
    extras = get_validator('convert_from_extras')
    single = get_validator('single_value')
    multi = get_validator('multi_value')

    original_schema.update({
        'title':                   [mandatory, single],
        'notes':                   [mandatory, single],
        'url':                     [optional, single],
        'theme':                   [extras, mandatory, multi()],

        'identifier':              [extras, mandatory, single],
        'alternate_identifier':    [extras, recommended, multi()],
        'source_catalog':          [extras, recommended, single],

        'authority':               [extras, mandatory, single],
        'publisher':               [extras, mandatory, single],
        'contact_point_name':      [extras, mandatory, single],
        'contact_point_email':     [extras, recommended, single],
        'contact_point_website':   [extras, recommended, single],
        'contact_point_phone':     [extras, recommended, single],

        'metadata_language':       [extras, mandatory, single],
        'language':                [extras, mandatory, multi()],

        'license_id':              [mandatory, single],
        'access_rights':           [extras, recommended, single],

        'temporal_label':          [extras, recommended, single],
        'temporal_start':          [extras, recommended, single],
        'temporal_end':            [extras, recommended, single],

        'spatial_scheme':          [extras, recommended,
                                    multi(allow_duplicates=True)],
        'spatial_value':           [extras, recommended, multi()],

        'legal_foundation_label':  [extras, recommended, single],
        'legal_foundation_ref':    [extras, recommended, single],
        'legal_foundation_uri':    [extras, recommended, single],

        'dataset_status':          [extras, recommended, single],
        'date_planned':            [extras, optional, single],
        'issued':                  [extras, recommended, single],
        'modified':                [extras, mandatory, single],
        'frequency':               [extras, recommended, single],

        'documentation':           [extras, optional, multi()],
        'sample':                  [extras, optional, multi()],
        'provenance':              [extras, optional, multi()],

        'version':                 [optional, single],
        'version_notes':           [extras, optional, multi()],

        'is_version_of':           [extras, optional, multi()],
        'has_version':             [extras, optional, multi()],
        'source':                  [extras, optional, multi()],
        'related_resource':        [extras, optional, multi()],
        'conforms_to':             [extras, optional, multi()],

        'high_value':              [extras, recommended, single],
        'referentie_data':         [extras, recommended, single],
        'basis_register':          [extras, recommended, single],
        'national_coverage':       [extras, recommended, single],

        'changetype':              [extras, recommended, single]
    })

    original_schema['resources'].update({
        'url':                     [mandatory, single],
        'download_url':            [recommended, multi()],
        'documentation':           [recommended, multi()],
        'linked_schemas':          [recommended, multi()],

        'name':                    [mandatory, single],
        'description':             [mandatory, single],
        'status':                  [recommended, single],

        'metadata_language':       [mandatory, single],
        'language':                [mandatory, multi()],

        'license_id':              [mandatory, single],
        'rights':                  [recommended, single],

        'format':                  [mandatory, single],
        'media_type':              [recommended, single],

        'size':                    [recommended, single],

        'hash':                    [optional, single],
        'hash_algorithm':          [optional, single],

        'release_date':            [recommended, single],
        'modification_date':       [recommended, single],

        'distribution_type':       [recommended, single]
    })

    return original_schema


def _mutate_schema(schema, changetype):
    mandatory = get_validator('not_empty')
    recommended = get_validator('ignore_empty')
    optional = get_validator('ignore_empty')
    extras = get_converter('convert_to_extras')
    single = get_validator('single_value')
    multi = get_validator('multi_value')
    string = get_validator('is_string')
    boolean = get_validator('is_bool')
    uri = get_validator('is_uri')
    date = get_validator('is_date')
    number = get_validator('is_number')
    in_list = get_validator('in_list')
    default = get_converter('default_conversion')
    res_to_string = get_converter('convert_list_to_string')
    contact_point = get_validator('contact_point')
    temporal = get_validator('temporal')
    legal_foundation = get_validator('legal_foundation')
    spatial = get_validator('spatial')
    rights = get_validator('rights')
    checksum = get_validator('checksum')
    date_planned = get_validator('date_planned')
    date_format = '%Y-%m-%dT%H:%M:%S'

    schema.update({
        'title':                   [mandatory, single, string],
        'notes':                   [mandatory, single, string],
        'url':                     [optional, single, uri],
        'theme':                   [mandatory, multi(),
                                    in_list('Overheid:Taxonomiebeleidsagenda'),
                                    extras],
        'identifier':              [mandatory, single, uri, extras],
        'alternate_identifier':    [recommended, multi(), uri, extras],
        'source_catalog':          [recommended, single,
                                    in_list('DONL:Catalogs'), extras],

        'authority':               [mandatory, single,
                                    in_list('DONL:Organization'), extras],
        'publisher':               [mandatory, single,
                                    in_list('DONL:Organization'), extras],
        'contact_point_name':      [mandatory, single, string, extras],
        'contact_point_title':     [recommended, single, string, extras],
        'contact_point_address':   [recommended, single, string, extras],
        'contact_point_email':     [recommended, single, string, extras],
        'contact_point_website':   [recommended, single, uri, extras],
        'contact_point_phone':     [recommended, single, string, extras],

        'metadata_language':       [mandatory, single, in_list('DONL:Language'),
                                    extras],
        'language':                [mandatory, multi(),
                                    in_list('DONL:Language'), extras],

        'license_id':              [mandatory, single,
                                    in_list('DONL:License')],
        'access_rights':           [recommended, single,
                                    in_list('Overheid:Openbaarheidsniveau'),
                                    extras],

        'temporal_label':          [recommended, single, string, extras],
        'temporal_start':          [recommended, single, date(date_format),
                                    extras],
        'temporal_end':            [recommended, single, date(date_format),
                                    extras],

        'spatial_scheme':          [recommended, multi(allow_duplicates=True),
                                    in_list('Overheid:SpatialScheme'), extras],
        'spatial_value':           [recommended, multi(), extras],

        'legal_foundation_label':  [recommended, single, string, extras],
        'legal_foundation_ref':    [recommended, single, string, extras],
        'legal_foundation_uri':    [recommended, single, uri, extras],

        'dataset_status':          [recommended, single,
                                    in_list('Overheid:DatasetStatus'), extras],
        'date_planned':            [optional, single, date(date_format),
                                    extras],
        'issued':                  [recommended, single, date(date_format),
                                    extras],
        'modified':                [mandatory, single, date(date_format),
                                    extras],
        'frequency':               [recommended, single,
                                    in_list('Overheid:Frequency'), extras],

        'documentation':           [optional, multi(), uri, extras],
        'sample':                  [optional, multi(), uri, extras],
        'provenance':              [optional, multi(), uri, extras],

        'version':                 [optional, single, string],
        'version_notes':           [optional, multi(), string, extras],

        'is_version_of':           [optional, multi(), uri, extras],
        'has_version':             [optional, multi(), uri, extras],
        'source':                  [optional, multi(), uri, extras],
        'related_resource':        [optional, multi(), uri, extras],
        'conforms_to':             [optional, multi(), uri, extras],

        'changetype':              [default(changetype, force=True),
                                    in_list('ADMS:Changetype'), extras],

        'high_value':              [default(False), single, boolean, extras],
        'referentie_data':         [default(False), single, boolean, extras],
        'basis_register':          [default(False), single, boolean, extras],
        'national_coverage':       [default(False), single, boolean, extras],

        '__after':                 [contact_point, temporal, legal_foundation,
                                    spatial, date_planned, rights]
    })

    schema['resources'].update({
        'url':                     [mandatory, single, uri],
        'download_url':            [recommended, multi(), uri, res_to_string],
        'documentation':           [recommended, multi(), uri, res_to_string],
        'linked_schemas':          [recommended, multi(), uri, res_to_string],

        'name':                    [mandatory, single, string],
        'description':             [mandatory, single, string],
        'status':                  [recommended, single,
                                    in_list('ADMS:DistributieStatus')],

        'metadata_language':       [mandatory, single,
                                    in_list('DONL:Language')],
        'language':                [mandatory, multi(),
                                    in_list('DONL:Language'), res_to_string],

        'license_id':              [mandatory, single,
                                    in_list('DONL:License')],
        'rights':                  [recommended, single, string],

        'format':                  [mandatory, single, 
                                    in_list('MDR:FiletypeNAL')],
        'media_type':              [recommended, single,
                                    in_list('IANA:Mediatypes')],

        'size':                    [recommended, single, number],

        'hash':                    [optional, single, string],
        'hash_algorithm':          [optional, single, string],

        'release_date':            [recommended, single, date(date_format)],
        'modification_date':       [recommended, single, date(date_format)],

        'distribution_type':        [recommended, single,
                                     in_list('DONL:DistributionType')],

        '__after':                 [checksum]
    })

    return schema
