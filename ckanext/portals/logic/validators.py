# encoding: utf-8


from ckan.plugins.toolkit import StopOnError, get_validator
from ckanext.portals.logic.helpers.config import get_config, in_list as in_code_list
from datetime import datetime
from dateutil import parser
from urlparse import urlparse
import re


def single_valued(key, data, errors, context): # noqa
    errors[key] = [] if not errors[key] else errors[key]

    if isinstance(data[key], list):
        errors[key].append('value must be single valued')
        raise StopOnError


def multi_valued(allow_duplicates=False):
    def multi_valued_validator(key, data, errors, context):
        get_validator('convert_string_to_list')(key, data, errors, context)
        errors[key] = [] if not errors[key] else errors[key]

        if not data[key]:
            return

        value = data[key]

        if isinstance(value, basestring):
            data[key] = [value]

        if not isinstance(value, list):
            errors[key].append('value must be multi valued')
            raise StopOnError

        contains_duplicates = len(value) != len(set(value))

        if not allow_duplicates and contains_duplicates:
            errors[key].append('duplicate values are not allowed')
            raise StopOnError

    return multi_valued_validator


def string(key, data, errors, context): # noqa
    if not data[key]:
        return

    error_message = 'expected a string, or a list of strings'
    errors[key] = [] if not errors[key] else errors[key]

    if isinstance(data[key], basestring):
        return

    if isinstance(data[key], list):
        for val in data[key]:
            if not isinstance(val, basestring):
                errors[key].append(error_message)
                raise StopOnError

        return

    errors[key].append(error_message)
    raise StopOnError


def boolean(key, data, errors, context): # noqa
    if not data[key]:
        return

    acceptable_values = [True, 'True', 'true', False, 'False', 'false']
    error_message = 'expected a boolean, or a list of boolean'
    errors[key] = [] if not errors[key] else errors[key]

    if isinstance(data[key], list):
        for val in data[key]:
            if val not in acceptable_values:
                errors[key].append(error_message)
                raise StopOnError

        return

    if not data[key] in acceptable_values:
        errors[key].append(error_message)
        raise StopOnError


def uri(key, data, errors, context): # noqa
    if not data[key]:
        return

    error_message = 'expected an URI, or a list of URI\'s'
    errors[key] = [] if not errors[key] else errors[key]

    if isinstance(data[key], list):
        for input_uri in data[key]:
            parsed = urlparse(input_uri)

            if not all([parsed.scheme, parsed.netloc]):
                errors[key].append(error_message)
                raise StopOnError

        return

    parsed = urlparse(data[key])

    if not all([parsed.scheme, parsed.netloc]):
        errors[key].append(error_message)
        raise StopOnError


def date(datetime_format):
    def valid_date(key, data, errors, context): # noqa
        if not data[key]:
            return

        error_message = 'expected a datetime, or a list of datetimes'
        errors[key] = [] if not errors[key] else errors[key]

        if isinstance(data[key], list):
            for given_date in data[key]:
                try:
                    datetime.strptime(given_date, datetime_format)
                except ValueError:
                    errors[key].append(error_message)

            return

        try:
            datetime.strptime(data[key], datetime_format)
        except ValueError:
            if datetime_format != '%Y-%m-%dT%H:%M:%S':
                errors[key].append(error_message)

                return

            try:
                datetime.strptime(data[key], '%Y-%m-%d')
                data[key] = '{0}T00:00:00'.format(data[key])
            except ValueError:
                errors[key].append(error_message)

        return

    return valid_date


def number(key, data, errors, context): # noqa
    if not data[key]:
        return

    error_message = 'expected an integer, or a list of integers'
    errors[key] = [] if not errors[key] else errors[key]

    if isinstance(data[key], list):
        for given_number in data[key]:
            try:
                if not int(given_number) > 0:
                    errors[key].append(error_message)
                    raise StopOnError
            except ValueError:
                errors[key].append(error_message)
                raise StopOnError

        return

    try:
        if not int(data[key]) > 0:
            errors[key].append(error_message)
            raise StopOnError
    except ValueError:
        errors[key].append(error_message)
        raise StopOnError

    return


def in_list(name):
    def in_list_validator(key, data, errors, context): # noqa
        input_values = data.get(key, None)

        error_message = 'value is not part of list ' + name
        errors[key] = [] if not errors[key] else errors[key]

        if not input_values:
            return

        if isinstance(input_values, list):
            [errors[key].append(error_message)
             for input_value in input_values if
             not in_code_list(name, input_value)]

            return

        if not in_code_list(name, str(input_values)):
            errors[key].append(error_message)

        return

    return in_list_validator


def contact_point(key, data, errors, context): # noqa
    message = 'website, email or phone is required for a valid contact_point'
    properties = [
        ('contact_point_website',),
        ('contact_point_email',),
        ('contact_point_phone',)
    ]

    [errors[prop].append(message) for prop in properties if
     not any([cp_property in data for cp_property in properties])]

    return


def temporal(key, data, errors, context): # noqa
    message = 'temporal_start cannot be greater or equal to temporal_end'
    properties = [('temporal_start',), ('temporal_end',)]
    properties_present = (prop in data for prop in properties)
    errors_present = (len(errors[prop]) > 0 for prop in properties)

    if not all(properties_present) or any(errors_present):
        return

    temporal_start = parser.parse(data[('temporal_start',)])
    temporal_end = parser.parse(data[('temporal_end',)])

    [errors[prop].append(message)
     for prop in properties if temporal_start >= temporal_end]

    return


def date_planned(key, data, errors, context):
    message = 'date_planned is required for the given dataset_status value'
    properties = [('dataset_status',), ('date_planned',)]
    errors_present = (len(errors[prop]) > 0 for prop in properties)
    affected_states = get_config('validation')['date_planned_states']

    if not properties[0] in data or any(errors_present):
        return key, data, errors, context

    try:
        data.get(properties[1])
    except KeyError:
        [errors[properties[1]].append(message) for data[properties[0]] in
         affected_states]

    return key, data, errors, context


def legal_foundation(key, data, errors, context): # noqa
    error_message = '{0} is required when providing a legal_foundation'
    properties = [
        ('legal_foundation_ref',),
        ('legal_foundation_uri',),
        ('legal_foundation_label',)
    ]
    properties_present = (prop in data for prop in properties)
    errors_present = (len(errors[prop]) > 0 for prop in properties)

    if not any(properties_present) or any(errors_present):
        return

    [errors[absentee].append(error_message.format(absentee[0]))
     for absentee in [prop for prop in properties if prop not in data]]

    return


def checksum(key, data, errors, context): # noqa
    hash_tuple = ('resources', key[1], 'hash')
    algorithm_tuple = ('resources', key[1], 'hash')

    if hash_tuple in data and data[hash_tuple] == '':
        data.pop(hash_tuple, None)
        errors.pop(hash_tuple, None)

    error_message = 'field is required when providing a valid checksum'
    properties = [hash_tuple, algorithm_tuple]
    properties_present = (prop in data for prop in properties if prop)

    if not any(properties_present):
        return

    if not all(properties_present):
        [errors[prop].append(error_message) for prop in properties]


def rights(key, data, errors, context): # noqa
    dataset_status = data.get(('dataset_status',), None)
    access_rights = data.get(('access_rights',), None)
    license = data.get(('license_id',), None)

    validation_config = get_config('validation')
    beschikbaar = 'http://data.overheid.nl/status/beschikbaar'
    public = 'http://publications.europa.eu/resource/authority/access-right/' \
             'PUBLIC'

    status_is_available = dataset_status == beschikbaar
    access_rights_public = access_rights == public
    license_is_open = license not in validation_config['non_open_licenses']

    if status_is_available and access_rights_public and not license_is_open:
        errors[('license_id',)] = [
            'When dataset_status is set to ' + beschikbaar + ' and '
            'access_rights is set to ' + public + ', an open license must be '
            'provided'
        ]


def spatial(key, data, errors, context): # noqa
    properties = [('spatial_scheme',), ('spatial_value',)]
    properties_present = (prop in data for prop in properties)

    if not any(properties_present):
        return

    if any((len(errors[prop]) > 0 for prop in properties)):
        return

    if not all(properties_present):
        [errors[absentee].append(absentee[0] + ' is required for spatial')
         for absentee in [prop for prop in properties if prop not in data]]

        return

    schemes = data[('spatial_scheme',)]
    values = data[('spatial_value',)]

    if len(schemes) != len(values):
        [errors[prop].append('the list of schemes must be of the same size as '
                             'the list of values')
         for prop in properties]

        return

    spatial_config = get_config('validation')['spatial']
    validator_mapping = {}

    for scheme_uri, mapping in spatial_config.iteritems():
        validator = mapping['validator']

        try:
            validator = validator(mapping['argument'])
        except KeyError:
            pass

        validator_mapping.update({scheme_uri: validator})

    message = 'spatial_value {0} is not valid according to scheme {1}'
    [errors[('spatial_value',)].append(message.format(val, schemes[iterator]))
     for iterator, val in enumerate(values) if
     not _valid_spatial(validator_mapping[schemes[iterator]], val)]


def _valid_spatial(validation_method, value):
    errors = {'value': []}
    validation_method('value', {'value': value}, errors, {})

    return len(errors['value']) == 0


def epsg_28992(key, data, errors, context): # noqa
    if key not in data:
        return

    regex_pattern = '^\d{6}(\.\d{3})? \d{6}(\.\d{3})?$' # noqa
    error_message = 'not a valid EPSG28992 value'
    errors[key] = [] if not errors[key] else errors[key]
    _regex_match(key, data[key], regex_pattern, errors, error_message)


def postcode_huisnummer(key, data, errors, context): # noqa
    if key not in data:
        return

    pattern = '^[1-9]\d{3}([A-Z]{2}(\d+(\S+)?)?)?$' # noqa
    error_message = 'value is not a valid Overheid:PostcodeHuisnummer'
    errors[key] = [] if not errors[key] else errors[key]
    _regex_match(key, data[key], pattern, errors, error_message)


def _regex_match(key, value, pattern, errors, error_message):
    if isinstance(value, list):
        [errors[key].append(error_message) for val in value
         if not re.match(pattern, val)]

        return

    if not re.match(pattern, value):
        errors[key].append(error_message)
