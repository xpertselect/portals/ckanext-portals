# encoding: utf-8


import ckan.plugins.toolkit as tk


def dataset_purge_authorization(context, package=None):
    user = context['user']
    user_to_user_id = tk.get_converter('convert_user_name_or_id_to_id')
    package_show = tk.get_action('package_show')
    organization_list_for_user = tk.get_action('organization_list_for_user')

    unauthorized = {
        'success': False,
        'msg': 'You must be authorized to perform this action'
    }

    try:
        user_id = user_to_user_id(user, context)
        user_organizations = organization_list_for_user(context, {
            'id': user_id,
            'permission': 'admin'
        })

        package = package_show(context, {'id': package['id']})
        user_organizations = [organization['id'] for organization
                              in user_organizations]

        if package['owner_org'] in user_organizations:
            return {'success': True}

        return unauthorized
    except tk.Invalid:
        return unauthorized
    except tk.ObjectNotFound:
        return {
            'success': False,
            'msg': 'No package exists with the given id'
        }
