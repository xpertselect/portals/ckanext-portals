# encoding: utf-8


def convert_string_to_list(key, data, errors, context): # noqa
    value = data.get(key, None)

    if not value:
        return

    if not isinstance(value, basestring):
        return

    if not value.startswith('{') or not value.endswith('}'):
        return

    value = value.replace('"', '')
    data[key] = value[1:len(value)-1].split(',')


def convert_list_to_string(key, data, errors, context): # noqa
    value = data.get(key, None)

    if not value:
        return

    if not isinstance(value, list):
        return

    data[key] = '{' + ','.join(map(str, value)) + '}'


def default(default_value, force=False):
    def default_setter(value):
        return value if value and not force else default_value

    return default_setter
