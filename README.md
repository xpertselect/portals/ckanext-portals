# XpertSelect / Portals / CkanExt Portals

[gitlab.com/xpertselect/portals/ckanext-portals](https://gitlab.com/xpertselect/portals/ckanext-portals)

A CKAN extension that implements a dataset schema based on DCAT-AP-DONL 1.1 (Finely tuned for the XpertSelect Portals stack) into CKAN.

## License

View the `LICENSE.md` file for licensing details.

## Requirements

This extensions has the following requirements to function correctly:

- Python version `^2.7`
- CKAN core version `^2.8`
- A Solr collection based on the `portals_ckan` configset available via [gitlab.com/xpertselect/portals/solr-configsets](https://gitlab.com/xpertselect/portals/solr-configsets)

## Installation

This extension is installed like any other CKAN extension:

```shell
export PORTALS_VERSION="1.0.0"
export CKAN_VENV=/path/to/venv

${CKAN_VENV}/bin/pip install \
  --no-cache-dir \
  --editable "git+https://gitlab.com/xpertselect/portals/ckanext-portals.git@${PORTALS_VERSION}#egg=ckanext-portals"
${CKAN_VENV}/bin/pip install \
  --no-cache-dir \
  --requirement "${CKAN_VENV}/src/ckanext-portals/requirements.txt"
```

Afterwards, include the following plugins in the `ckan.plugins` located in the `ckan.ini` file:

```ini
ckan.plugins = portals-scheme portals-authorization portals-interface
```
